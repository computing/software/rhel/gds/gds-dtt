%define name 	gds-dtt
%define version 2.19.5
%define release 4
%define daswg   /usr
%define prefix  %{daswg}
%define gds_python_version 3.6
%define gds_python_version_nodots 36
%define config_pygds --enable-python

%define dmtrun 1
%if %{dmtrun}
%define _sysconfdir /etc
%define config_runflag --enable-dmt-runtime --sysconfdir=%{_sysconfdir}
%else
%define config_runflag --disable-dmt-runtime
%endif

%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

%define root_cling %(test "`root-config --has-cling`" != "yes"; echo $?)

Name: 		     %{name}
Summary: 	     gds-dtt 2.19.5
Version: 	     %{version}
Release: 	     %{release}%{?dist}
License: 	     GPL
Group: 		     LIGO Global Diagnotic Systems SWIG bindings
Source: 	     https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager:      John Zweizig (john.zweizig@ligo.org)
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
URL: 		       http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: automake, autoconf, libtool, m4, make
BuildRequires: gzip bzip2 expat-devel libXpm-devel
BuildRequires: root
BuildRequires: python3 python3-devel python36-numpy
BuildRequires: python python-devel python2-numpy
BuildRequires: libmetaio-devel
BuildRequires: readline-devel fftw-devel
BuildRequires: jsoncpp-devel
BuildRequires: gds-base-devel >= 2.19.5
BuildRequires: gds-lsmp-devel >= 2.19.7
BuildRequires: gds-frameio-devel >= 2.19.5
BuildRequires: gds-gui-devel >= 2.19.4
BuildRequires: gds-pygds >= 2.19.4
Requires: gds-base >= 2.19.5
Requires: gds-lsmp >= 2.19.7
Requires: gds-frameio-base >= 2.19.5
Requires: gds-gui >= 2.19.4
Requires: gds-pygds >= 2.19.4
Prefix:		     %prefix

%description
Global diagnostics software DTT bindings

%package   headers
Summary: 	 GDS DTT header files.
Version: 	 %{version}
Group: 		 LSC Software/Data Analysis
Requires:  gds-base-headers >= 2.19.5

%description headers
GDS software header files.

%package  devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name}-headers = %{version}-%{release}
Requires: %{name} = %{version}-%{release}
Requires: expat-devel

%description devel
GDS software development files.

%package crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}
Requires: %{name}-headers = %{version}-%{release}

%description crtools
GDS DTT control room tools

%package  monitors
Summary: 	GDS DTT Libraries used by DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}

%description monitors
GDS DTT Libraries used by DMT Monitor programs

%package -n py%{gds_python_version_nodots}-gds-dtt
Summary: 	Python wrapper and utilities for gds dtt library
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}
Requires: gds-frameio-base >= 2.19.5
Obsoletes: py-gds-dtt < 2.19.5

%description -n py%{gds_python_version_nodots}-gds-dtt
Python wrappers and utilities for gds dtt library

#
#
#

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure \
    PYTHON=/usr/bin/python%{gds_python_version} \
    PYTHON_VERSION=%{gds_python_version} \
    --prefix=%prefix --libdir=%{_libdir} \
	  --includedir=%{prefix}/include/gds \
	  --enable-online --enable-dtt %{config_pygds} %{config_runflag}
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files

%files devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/*
%{_libdir}/*.a

%files headers
%defattr(-,root,root)
%{_includedir}/gds

%files crtools
%defattr(-,root,root)
%{_bindir}/CY.robot
%{_bindir}/fantom
%{_bindir}/udnls
%{_bindir}/awggui
%{_bindir}/awgexc_run
%{_bindir}/awgstream
%{_bindir}/chndump
%{_bindir}/diag
%{_bindir}/diagd
%{_bindir}/diaggui
%{_bindir}/gdserrd
%{_bindir}/lidax
%{_bindir}/multiawgstream
%{_bindir}/tpcmd
%{_bindir}/xmlconv
%{_bindir}/xmldata
%{_bindir}/xmldir
%{_libdir}/libawg.so*
%{_libdir}/libdfm.so*
%{_libdir}/libdfmgui.so*
%{_libdir}/libdtt.so*
%{_libdir}/libfantom.so*
%{_libdir}/liblidax.so*
%{_libdir}/libSIStr.so*
%{_libdir}/libtestpoint.so*
%{_datadir}/gds/startup/*
%{_mandir}/*

%files monitors
%defattr(-,root,root)
%{_bindir}/InspiralRange
%{_datadir}/gds/man

# These should be in a python specfic package
%files -n py%{gds_python_version_nodots}-gds-dtt
%{python3_sitelib}/*

%changelog
* Wed May 19 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Updated for release as described in NEWS.md

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Updated for release as described in NEWS.md

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Split lowlatency as a separate package
